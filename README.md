Python Wrapper for ScaLapack
============================

Installation
------------

* Prerequisites
    - Python
    - MPI
    - Blacs, ScaLapack
    - mpi4py (pip install mpi4py)

* Install

        cd pyscalapack
        cp customize.py.arch customize.py (you may need to edit it)
        cd ..
        python setup.py install


There is few custom.py.arch as example, you will probably need to edit it as function of
your system. This file consist of a dictionary that define include directories and libraries
as well than compilers. For example,

        lib_info = {'libraries': ["blas", "blacsCinit-openmpi","blacs-openmpi", "scalapack-openmpi"],
                    'library_dirs': ["/usr/lib"],
                    'define_macros': [], 
                    'include_dirs': ["/usr/include"]
                   }

        compilers = {"CC": "mpicc",
                     "FC": "mpif90",
                     "CXX": "mpic++"
                    }

Documentation
-------------
Documentation is available at http://mbarbry.pagekite.me/PyScalapack/

Notes
-----
* Complex version of p?gemv and p?gemm does not compile
* To run tests
      
        cd test
        mpirun -np nprocs python test_??.py

* One need to be extremely careful with the mpirun executable that he is using. For
example, if you use Anaconda python package, and you installed PyScalapack in it, then
you must use the mpirun coming with anaconda, usually $HOME/anaconda/bin/mpirun (same goes for
intelpython).

Bug report
----------
Send bugs to Marc Barbry <marc.barbry@mailoo.org>

