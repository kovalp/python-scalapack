from __future__ import division, print_function, absolute_import
import sys
import subprocess
import os
from os.path import join

if sys.version_info[0] >= 3: # from Cython 0.14
    from distutils.command.build_py import build_py_2to3 as build_py
else:
    from distutils.command.build_py import build_py

def generate_src_file(fname, replace, outdir="."):
    """
    generate pyf file from pyf.src
    The dictionary replace can contain
        <type>: [list of types (real(4), complex(4) ... )]
        <prefix>: [list of prefix coresponding to type (s, c, ....)]
        <ctype>: [list of ctype coresponding to type (float, float_complex)]
    """
    
    inName = fname + ".src"
    with open(inName, "r") as in_file:
        template = in_file.read()

    outdir += "/"
    f = open(outdir + fname, "w")
    if isinstance(replace, dict):
        if len(replace) > 0:
            keys = list(replace.keys())
            for iv in range(len(replace[keys[0]])):
                new = template
                for k, v in replace.items():
                    new = new.replace(k, v[iv])
                f.write(new + "\n")
        else:
            f.write(template)
    else:
        raise ValueError("replace must be a dictionary!!")
    f.close()

def configuration(parent_package='', top_path=None):
    from distutils.sysconfig import get_python_inc
    from numpy.distutils.system_info import get_info, NotFoundError, numpy_info
    from numpy.distutils.misc_util import Configuration, get_numpy_include_dirs

    config = Configuration('pyscalapack', parent_package, top_path)

    sources = ["pyscalapack.pyf.src"]

    from customize import lib_info
    config.add_extension('pyscalapack',
             sources=sources,
             depends=["m_blacs.pyf.src", "m_pblas.pyf.src",
                      "m_scalapack.pyf.src", "m_scalapack_tools.pyf.src"],
             extra_info=lib_info)

    config.add_subpackage("tools")

    return config

if __name__ == '__main__':
    from numpy.distutils.core import setup
    
    setup(**configuration(top_path='').todict())
