from __future__ import division

def l2g(il, p, n, np, nb):
    """
        convert local index to global index in block-cyclic distribution

        Input Parameters:
        -----------------
            il (int) local array index
            p  (int) processor array index
            n  (int) global array dimension
            np (int) processor array dimension
            nb (int) block size (Warning: must be 1!!)

        Output Parameters:
        ------------------
            i  (int) global array index
    """
    
    if nb != 1:
        raise ValueError("index convertor works only for nb =1 at the moment!")

    return int((((il/nb) * np) + p)*nb + il%nb)

def g2l(i,n,np,nb):
    """
        convert global index to local index in block-cyclic distribution

        Input Parameters:
        -----------------
            i  (int) global array index
            n  (int) global array dimension
            np (int) processor array dimension
            nb (int) block size (Warning:  must be 1!!)

        Output Parameters:
        ------------------
            p  (int) processor array index
            il (int) local array index
    """

    if nb != 1:
        raise ValueError("index convertor works only for nb =1 at the moment!")

    return int((i/nb)%np), int((i/(np*nb))*nb + i%nb)
