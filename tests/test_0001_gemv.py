import unittest
from mpi4py import MPI
import numpy as np
from scipy.linalg.blas import dgemv
import pyscalapack.pyscalapack as pys
import sys
from pyscalapack.tools.m_utils import g2l

def gemv_mpi(comm, A_mpi, B_mpi, N, nb, order="F"):
    
    # initialise blacs
    iam, nprocs = pys.blacs_pinfo()

    nprow = int(np.sqrt(1.0*nprocs))
    npcol = int(nprocs/nprow)

    context = pys.blacs_get(-1)
    pys.blacs_gridinit(context, nprow, npcol)

    nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

    if (myrow < 0 or mycol < 0):
        raise ValueError("myrow < 0 or mycol < 0")

    # initialise temporary arrays on each thread
    myArows = pys.numroc(N, nb, myrow, 0, nprow)
    myAcols = pys.numroc(N, nb, mycol, 0, npcol)

    myBrows = pys.numroc(N, nb, myrow, 0, nprow)
    myBcols = 1

    ath = np.zeros((myArows, myAcols), dtype=np.float64, order=order)
    bth = np.zeros((myBrows, myBcols), dtype=np.float64, order=order)
    cth = np.zeros((myBrows, myBcols), dtype=np.float64, order=order)

    desca, info = pys.descinit(N, N, nb, nb, context, myArows)
    descb, info = pys.descinit(N, 1, nb, nb, context, myBrows)
    descc, info = pys.descinit(N, 1, nb, nb, context, myBrows)

    comm.Barrier()

    # Scatter A on each thread. Probably better to use MPI_Scatter !!
    for i in range(N):
        iproc, myi = g2l(i, N, nprow, nb)
        if myrow == iproc:
            for j in range(N):
                jproc, myj = g2l(j, N, npcol, nb)
                if mycol == jproc:
                    ath[myi, myj] = A_mpi[i, j]
    
    # Scatter B on each thread. Probably better to use MPI_Scatter !!
    global_index = np.zeros(myBrows, dtype=int)
    for i in range(N):
        iproc, myi = g2l(i, N, nprow, nb)
        if myrow == iproc:
            global_index[myi] = i
            bth[myi, 0] = B_mpi[i]

    # send local index into master thread for future gathering
    index_dic = {}
    comm.send(global_index, dest=0, tag=10+iam)
    if iam == 0:
        for i in range(nprocs):
            index_dic[i] = comm.recv(source=i, tag=10+i)

    comm.Barrier()
    
    # Perform matrix-vector multiplication
    cth = pys.pdgemv(N, N, 1.0, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descc)
    comm.Barrier()
    
    # For some reason that I did not understood, 1 node over 2 give cth = 0
    # Nevertheless, the final results is correct, and the input array is well
    # separated between the different threads. I think that, somehow, pdgemv
    # is already gathering the results on two threads, why???

    # this test check if the returned array from pdgemv is not only constituted of zeros
    allzeros = np.allclose(cth, np.zeros(cth.shape))
    allzeros_dict = {}
    comm.send(allzeros, dest=0, tag=10+iam)
    if iam == 0:
        for i in range(nprocs):
            allzeros_dict[i] = comm.recv(source=i, tag=10+i)

    # Print arrays For debugging
    #comm.Barrier()
    #for i in range(nprocs):
    #    comm.Barrier()
    #    if i == iam:
    #        print("rank = {0}: ath = \n".format(iam), ath)
    #        print("bth = \n", bth)
    #        print("cth = \n", cth)
 

    # initialize the C_mpi output on master node
    if iam == 0:
        C_mpi = np.zeros(N, dtype=np.float64, order=order)
    else:
        C_mpi = None
    comm.Barrier()

    # if not cth is not constituted of zeros, then send cth on the master thread
    if not allzeros:
        for ind, ci in enumerate(cth):
            comm.send(ci, dest=0)

    # gather the result in the master thread
    if iam == 0:
        for i in range(nprocs):
            if not allzeros_dict[i]:
                for ind, ci in enumerate(cth):
                    C_mpi[index_dic[i][ind]] = comm.recv(source=i)
    
    return C_mpi
    
    
class KnowValues(unittest.TestCase):

    def test_pnum_pcoord(self):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()

        N = 4           # size of the problem (4x4 matrix)
        nb = 1          # block size, only working with 1 for the moment ??
        order = "F"     # For the moment working only with colum-major


        A_mpi = np.zeros((N, N), dtype=np.float64, order=order)
        B_mpi = np.zeros(N, dtype=np.float64, order=order)

        # Initialize data
        if rank == 0:
            for i in range(N):
                B_mpi[i] = 0.2*i*np.random.randn(1)[0]
                for j in range(N):
                    A_mpi[i, j] = (i-0.5*j)*np.random.randn(1)[0]

            As = np.copy(A_mpi)
            Bs = np.copy(B_mpi)

            Cs = dgemv(1.0, As, Bs)
        # End solving serial problem

        # Bdcast arrays on each thread, we should avoid this operation for 
        # production code, 
        comm.Bcast(A_mpi)
        comm.Bcast(B_mpi)

        comm.Barrier()

        # Matrix-verctor multiplications with MPI 
        C_mpi = gemv_mpi(comm, A_mpi, B_mpi, N, nb, order="F")

        if rank == 0:
            assert(np.allclose(Cs, C_mpi))

        # pys.blacs_exit()

if __name__ == "__main__":
    print("Testing pblas p?gemv")
    unittest.main()
