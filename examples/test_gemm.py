"""
    Compute the eigenvalues and eigenvectors of a real generalized symmetric-definite
    eigenproblem of the form 
        A*x = lambda*B*x

    Test the mpi implementation
"""

from mpi4py import MPI
import numpy as np
from scipy.linalg.blas import dgemm
import pyscalapack.pyscalapack as pys
import sys


def gemm_mpi(A_mpi, B_mpi, N, nb, order="F"):
    # initialise mpi
    iam, nprocs = pys.blacs_pinfo()
    print("iam = {0}, nprocs =  {1}".format(iam, nprocs))

    nprow = int(np.sqrt(1.0*nprocs))
    npcol = int(nprocs/nprow)
    print("nprow, npcol = ", nprow, npcol)

    context = pys.blacs_get(-1)
    pys.blacs_gridinit(context, nprow, npcol)

    nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)
    print("nprow, npcol, myrow, mycol = ", nprow, npcol, myrow, mycol)

    if (myrow < 0 or mycol < 0):
        raise ValueError("myrow < 0 or mycol < 0")

    lda = pys.numroc(N, nb, myrow, 0, nprow)
    print("lda = ", lda)

    ath = np.zeros((lda, lda), dtype=np.float64, order=order)
    bth = np.zeros((lda, lda), dtype=np.float64, order=order)

    desca, info = pys.descinit(N, N, nb, nb, context, lda)
    descb, info = pys.descinit(N, N, nb, nb, context, lda)
    descc, info = pys.descinit(N, N, nb, nb, context, lda)

    comm.Barrier()

    for i in range(N):
        for j in range(N):
            pys.pdelset(ath, i+1, j+1, desca, A_mpi[i, j])
            pys.pdelset(bth, i+1, j+1, descb, B_mpi[i, j])

    for i in range(nprocs):
        comm.Barrier()
        if i == iam:
            print("rank = {0}: ath = \n".format(iam), ath)
#            print(bth)

    comm.Barrier()
    C_mpi = pys.pdgemm(1.0, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descc)

    for i in range(nprocs):
        comm.Barrier()
        if i == iam:
            print("rank = {0}: C_mpi = \n".format(iam), C_mpi)
    pys.blacs_exit()

    return C_mpi

comm = MPI.COMM_WORLD
rank = comm.Get_rank()


N = 4       # size of the problem (4x4 matrix)
nb = 1      # block size
order = "F"

A_mpi = np.zeros((N, N), dtype=np.float64, order=order)
B_mpi = np.zeros((N, N), dtype=np.float64, order=order)
C_ref = np.zeros((N, N), dtype=np.float64, order=order)

# Initialize data
if rank == 0:
    for i in range(N):
        for j in range(N):
            A_mpi[i, j] = (i-0.5*j)*np.random.randn(1)[0]
            B_mpi[i, j] = 0.2*(i-0.5)*(j+2)*np.random.randn(1)[0]

    As = np.copy(A_mpi)
    Bs = np.copy(B_mpi)

    C_ref = dgemm(1.0, As, Bs)
    print("ws =\n", C_ref)
    print("A = \n", A_mpi)
# End solving serial problem

comm.Bcast(A_mpi)
comm.Bcast(B_mpi)
comm.Bcast(C_ref)

comm.Barrier()
# Same diagonalization with Scalapack ... 
C_mpi = gemm_mpi(A_mpi, B_mpi, N, nb, order="F")

print("Error = ", np.sum(abs(C_mpi - C_ref)))
